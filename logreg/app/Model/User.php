<?php
    
    App::uses('AppModel', 'Model');
    App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

    class User extends AppModel {

        public $validate = array(
            'fullname' => array(
                'required' => array(
                    'rule' => array('notBlank'),
                    'message' => 'A fullname is required'
                )
            ),
            'username' => array(
                'required' => array(
                    'rule' => array('isUnique'),
                    'message' => 'username is already taken'
                )
            ),
            'password' => array(
                'required' => array(
                    'rule' => array('notBlank'),
                    'message' => 'Please enter your password'
                ),
                'Match passwords' => array(
                    'rule' => 'matchPasswords',
                    'message' => 'Your Passwords do not match!'
                )
            ),
            'password_confirmation' => array(
                'required' => array(
                    'rule' => array('notBlank'),
                    'message' => 'Please confirm your password'
                )
            ),
            'email' => array(
                'email' => array(
                    'rule'    => array('email', true),
                    'message' => 'Please supply a valid email address.'
                ),
                'required' => array(
                    'rule' => array('notBlank'),
                    'message' => 'A email is required'
                )
            ),
            'role' => array(
                'valid' => array(
                    'rule' => array('inList', array('admin', 'author')),
                    'message' => 'Please enter a valid role',
                    'allowEmpty' => false
                )
            )
        );

        public function beforeSave($options = array()) {
            if (isset($this->data[$this->alias]['password'])) {
                $passwordHasher = new SimplePasswordHasher();
                $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password']
                );
            }
            return true;
        }

        public function matchPasswords($data){
            if($data['password'] == $this->data['User']['password_confirmation']){
                return true;
            }
            return false;
        }
    }

?>