<div class="users form">
	<?php echo $this->Form->create('User'); ?>
		<fieldset>
			<legend><?php echo __('Add User'); ?></legend>
		<?php
			echo $this->Form->input('fullname');
			echo $this->Form->input('username');
			echo $this->Form->input('password');
			echo $this->Form->input('password_confirmation', array('type' => 'password'));
	        echo $this->Form->input('email');
	        echo $this->Form->input('phone');
	        echo $this->Form->input('role', array('options' => array('admin' => 'Admin', 'author' => 'Author')));
		?>
		</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li>Register your details.</li>
	</ul>
</div>
