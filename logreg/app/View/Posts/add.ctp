<div class="posts form">
	<?php echo $this->Form->create('Post'); ?>
		<fieldset>
			<legend><?php echo __('Add Post'); ?></legend>
		<?php
			$user = AuthComponent::user();

			echo $this->Form->input('title');
			echo $this->Form->input('body');
			echo $this->Form->hidden('user_id',array('value'=>$user['id']));
		?>
		</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Logout'), array('controller' => 'Users', 'action' => 'logout')); ?></li>
	</ul>
</div>
