<?php
	
	App::uses('AppController', 'Controller');

	class PostsController extends AppController {

		public $components = array('Paginator');

		public function index() {
			
            $posts = $this->Post->find('all',array('conditions' => array('Post.userid' => $this->Auth->user('id'))));
	           	$this->set(array(
	            'posts' => $posts,
	            '_serialize' => array('posts')
	        ));

			$userid = $this->Auth->user();
			$parameters =array(
				'conditions' => array(
					'userid' => $userid
				)
			);

			$this->loadModel('Post');
			$this->set('posts', $this->Paginator->paginate());
			$this->set('posts', $this->Post->find('all', $parameters));
		}

		public function isAuthorized($user) {
		    // All registered users can add posts
		    if ($this->action === 'add') {
		        return true;
		    }

		    // The owner of a post can edit and delete it
		    if (in_array($this->action, array('view', 'edit', 'delete'))) {
		        $postId = (int) $this->request->params['pass'][0];
		         if ($this->Post->isOwnedBy($postId, $user['id'])) {
		            return true;
		        }
		       
		    }
		    return parent::isAuthorized($user);
		}

	    public function visitors(){
	        $this->Post->recursive = 0;
	        $this->set('posts', $this->Paginator->paginate());
	    }

		public function view($id = null) {

			// $this->requestAction('posts/isAuthorized');

			if (!$this->Post->exists($id)) {
				throw new NotFoundException(__('Invalid post'));
			}
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$this->set('post', $this->Post->find('first', $options));
		}

		public function add() {
			if ($this->request->is('post')) {
				$this->request->data['Post']['userid'] = $this->Auth->user('id');
				$this->Post->create();
				if ($this->Post->save($this->request->data)) {
					$this->Session->setFlash(__('The post has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
				}
			}
		}

		public function edit($id = null) {
			if (!$this->Post->exists($id)) {
				throw new NotFoundException(__('Invalid post'));
			}
			if ($this->request->is(array('post', 'put'))) {
				if ($this->Post->save($this->request->data)) {
					$this->Session->setFlash(__('The post has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
				}
			} else {
				$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
				$this->request->data = $this->Post->find('first', $options);
			}
		}

		public function delete($id = null) {
			$this->Post->id = $id;
			if (!$this->Post->exists()) {
				throw new NotFoundException(__('Invalid post'));
			}
			$this->request->allowMethod('post', 'delete');
			if ($this->Post->delete()) {
				$this->Session->setFlash(__('The post has been deleted.'));
			} else {
				$this->Session->setFlash(__('The post could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}

?>